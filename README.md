# MultiDimecional

* Requerimientos
Instalar Visual Studio herramientas para Bussiness I.

Crear un nuevo proyect en VS de tipo BI categoria **Multidimentional Data**.

1. Crear **DataSource**
- Indicar el motor de base de datos a usar.
    Como recomendacion dar un usuario y contrase;a para el motor a usar al momento de crear la conexion 
- Selecionar la o las bases de datos a usar

2. Crear **DataSource View**
- Seleccionar las tablas a usar para para el modelo multidimesional

Se trabaja como para armar un reporte, los filtros que se tiene para filtrar los datos se les llaman **DIMESIONES**
Los valores numericos se lse llaman **MEDIDAS**

3. Creciones de Calculos
- Para crear datos se debe de dar click dentro de la tabla en la cual se hara el calculo
- Se pueden crear calculos que seran como nuevas columnas dentro se pueden hacer calculos o uniones u otros.

4. Creacion de Dimensiones
- Seleccionar en el folder Dimensiones y seleccionar la tabla que se usara para la dimension.

5. Creacion de Dimesiones de Tiempo
- Se pueden crear dimesiones basadas en tiempo. Estas pueden estar almacenadas dentro de  del Data Source o dentro del Server
- Estas crearan sus propios Hieranches
- Se debe de generar la tabla, de lo contrario no se presentara como aparte de las dimesiones ya que no 
tiene con que relacionar.

Nota: Las tablas de Tiempo generan una nueva vista donde se genera solamante la dimension creada. 
Sol: Importar la tabla 

6. Measure Tables --- Tablas de medida
- Sera la tabla central contra la que se compara. Es decir si las tablas de dimensiones se tienen relacionadas dentro del Modelo SQL
al momento de crear el Cubo se obtendran implicitamente

7. Creacion de Cubo
- Dentro del Folder de Grupo, click derecho e iniciar Wizzard. 
- Seleccionar Dimesiones que se quiere tener para ese Cubo.
- Crear el Cubo
- Si alguno no se pudo obtener, se puede agregar dando click derecho en las propiedades y manualmente guardar el recorido de las dimesiones.

8. Build Dim
- Se puede generar una dimension haciendo que se lanzen los datos necesitados y presentandolos como una previa.

Las dimensiones pueden generar gerarquias, se puede arrastrar campos la generar un dato dentro del otro, es decir, si ingresar dentro de
cada columna. Esto para tener un mejor control de los filtros.

Debera de crear una dimension por cada filtro del reporte a realizar